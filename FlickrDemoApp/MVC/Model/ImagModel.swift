//
//  ImagModel.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import UIKit

class SearchModel: Codable {
    var photos: Photos
}

class Photos: Codable{
    var photo: [ImageModel] = []
}

class ImageModel: Codable {
    
    var title: String?
    var id: String?
    var farmID: Int?
    var serverID: String?
    var secretID: String?
    var imgURL: URL? {
        if let farm = farmID, let server = serverID, let secret = secretID, let id = id {
            return URL.init(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg")
        }else{
            return nil
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case title
        case id
        case farmID = "farm"
        case serverID = "server"
        case secretID = "secret"
    }
}

//SAMPLE URL
// https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_size.jpg
