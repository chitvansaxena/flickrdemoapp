//
//  HomeVCViewController.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

    //MARK: OUTLETS
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var arrImages: [ImageModel] = []
    
    //MARK: LIFECYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScreen()
    }
    
    //MARK: CUSTOM METHODS
    func setupScreen(){
        searchBar.delegate = self
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
        tableView.tableFooterView = UIView()
    }
    
    func updateTable(){
        if arrImages.count > 0 {
            tableView.isHidden = false
            tableView.reloadData()
        }else{
            tableView.isHidden = true
        }
    }
    
    //MARK: API METHODS
    func apiCall(textToSearch: String?) {
        guard let text = textToSearch else {return}
        APIManager.shared.request(searchString: text, isLoaderRequired: true, response: {[unowned self] (response) in
            self.arrImages = response
            DispatchQueue.main.async {
                self.updateTable()
            }
        }) {(error) in
            Alert.error.showDismissAlert(message: error.localizedDescription)
        }
        
    }
}

//MARK: SEARCHBAR DELEGATE
extension HomeVC: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        apiCall(textToSearch: searchBar.text)
    }
}
//MARK: UITABLEVIEW DATASOURCE
extension HomeVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifier.flickerCell.rawValue) as? FlickrCell else {return UITableViewCell()}
        cell.update(model: arrImages[indexPath.row])
        return cell
    }
}

//MARK: UITABLEVIEW DELEGATE
extension HomeVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: ViewControllerIdentifier.fullScreenVC.rawValue) as? FullScreenVC else {return}
        vc.imgModel = arrImages[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

