//
//  FullScreenVC.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import UIKit
import Kingfisher

class FullScreenVC: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var imgModel : ImageModel = ImageModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateScreen()
    }
    
    func updateScreen(){
        imageView.kf.setImage(with: imgModel.imgURL)
        lblTitle.text = imgModel.title
    }
    
    //MARK: ACTION METHODS
    @IBAction func btnBack(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
}
