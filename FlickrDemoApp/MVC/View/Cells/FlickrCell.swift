//
//  FlickrCell.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import UIKit

class FlickrCell: UITableViewCell {

    //MARK: OUTLETS
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var presentModel: ImageModel = ImageModel() {
        didSet{
            if let url = presentModel.imgURL {
                imgView.kf.indicatorType = .activity
                imgView.kf.setImage(with: url)
            }
            lblTitle.text = presentModel.title
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: CUSTOM METHODS
    func update(model: ImageModel) {
        self.presentModel = model
    }

}
