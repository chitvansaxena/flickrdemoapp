//
//  Constants.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation

//MARK: CONSTANTS
enum Constants: String {
    case apiKey = "6f4fe9f6eb7b1a8946e50f577a171f03"
    case apiBasePath = "https://api.flickr.com/services/rest/?method=flickr.photos.search"
}

//MARK: CELL IDENTIFIERS
enum CellIdentifier: String {
    case flickerCell = "FlickrCell"
}

//MARK: VCIDENTIFIERS
enum ViewControllerIdentifier: String {
    case fullScreenVC = "FullScreenVC"
}

//MARK: API KEYS
enum APIKeys: String {
    
    case photos = "photos"
    case arrPhoto = "photo"
    case id = "id"
    case title = "title"
    case farm = "farm"
    case server = "server"
    case secret = "secret"
}

//MARK: ERROR MESSAGES
enum ErrorMessages: String {
    case dismiss = "Dismiss"
}
