//
//  APIManager.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation

class APIManager: NSObject {
    
    static let shared = APIManager()
    
    func request(searchString: String, isLoaderRequired: Bool, response: @escaping ([ImageModel]) -> Void,errorOccured: @escaping(Error) -> Void){
        
        guard let url = SearchEndPoint.photoSearch.finaURL(searchString) else {return}
        var request = URLRequest.init(url: url)
        request.httpMethod = "GET"
        
        let session = URLSession.shared
        ActivityIndicator.shared.startAnimation()
        let apiCall = session.dataTask(with: request) { (dataResponse, urlResponse, error) in
            ActivityIndicator.shared.stopAnimation()
            if let error = error {
                debugPrint(error.localizedDescription)
                errorOccured(error)
            } else{
                guard let jsonResponse = dataResponse else {return}
                do {
                    let decoder = JSONDecoder()
                    let result = try decoder.decode(SearchModel.self, from: jsonResponse)
                    response(result.photos.photo)
                }catch{
                    debugPrint(error)
                    return
                }
            }
        }
        apiCall.resume()
    }
}
