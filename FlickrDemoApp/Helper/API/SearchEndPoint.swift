//
//  SearchEndPoint.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/15/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation

enum SearchEndPoint {
    
    case photoSearch
    
    var basePath: String {
        return Constants.apiBasePath.rawValue
    }
    
    var apiString: String {
        return "&api_key=" + Constants.apiKey.rawValue
    }
    
    var jsonFormatParams: String {
        return "&format=json&nojsoncallback=1"
    }
    
    var textParam: String {
        return "&text="
    }
    
    func finaURL(_ textToSearch: String) -> URL? {
        switch  self {
        case .photoSearch:
            let urlString = basePath + apiString + jsonFormatParams + textParam + textToSearch
            return URL.init(string: urlString)
        default:
            break
        }
    }
}
