//
//  UIImageViewExtension.swift
//  FlickrDemoApp
//
//  Created by S Chitvan  on 11/14/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation
import UIKit

enum Alert: String{
    
    case error = "Error"
    
    func showDismissAlert(message: String){
        let alert = UIAlertController.init(title: self.rawValue, message: message, preferredStyle: .alert)
        let action = UIAlertAction.init(title: ErrorMessages.dismiss.rawValue , style: .cancel) { (action) in
        }
        alert.addAction(action)
        UIApplication.shared.keyWindow?.topMostVC?.present(alert, animated: true, completion: nil)
    }
}
