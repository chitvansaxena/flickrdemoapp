//
//  ActivityIndicator.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/15/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation
import UIKit

class ActivityIndicator {
    
    static let shared = ActivityIndicator()
    
    var spinner = UIActivityIndicatorView()
    
    func startAnimation(){
        guard let view = UIApplication.shared.keyWindow?.topMostVC?.view else {return}
        spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height:50))
        spinner.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        spinner.layer.cornerRadius = 3.0
        spinner.clipsToBounds = true
        spinner.hidesWhenStopped = true
        spinner.style = UIActivityIndicatorView.Style.white;
        spinner.center = view.center
        view.addSubview(spinner)
        spinner.startAnimating()
        DispatchQueue.main.async {
            UIApplication.shared.beginIgnoringInteractionEvents()
            view.addSubview(self.spinner)
        }
    }
    
    func stopAnimation(){
        DispatchQueue.main.async {
            self.spinner.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
        }
    }
}
