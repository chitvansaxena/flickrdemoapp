//
//  UIWindowExtension.swift
//  FlickrDemoApp
//
//  Created by IOS Developer on 11/15/18.
//  Copyright © 2018 IOS Developer. All rights reserved.
//

import Foundation
import UIKit

extension UIWindow {
    
    //Return the top Most Controller 
    var topMostVC : UIViewController? {
        var top = self.rootViewController
        while true {
            if let presented = top?.presentedViewController {
                top = presented
            } else if let nav = top as? UINavigationController {
                top = nav.visibleViewController
            } else if let tab = top as? UITabBarController {
                top = tab.selectedViewController
            } else {
                break
            }
        }
        return top
    }
}
